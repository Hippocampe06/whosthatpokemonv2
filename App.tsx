import { StatusBar } from "expo-status-bar";
import { StyleSheet, Text, TouchableOpacity } from "react-native";
import React, { useEffect, useState } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

import { BottomBar } from "./src/components";
import useFirebaseLogin from "./src/hooks/useFirebaseLogin";
import { initializeApp } from "firebase/app";
import {
  Connection,
  Deck,
  Fight,
  LooseScreen,
  Register,
  WinScreen,
} from "./src/pages";
import { MaterialIcons } from "@expo/vector-icons";

const firebaseConfig = {
  apiKey: "AIzaSyAcI6MbtSMzIz1XtRzfeAV2yY6dk9Teakw",
  authDomain: "whosthatpokemonv2.firebaseapp.com",
  projectId: "whosthatpokemonv2",
  storageBucket: "whosthatpokemonv2.appspot.com",
  messagingSenderId: "67906114245",
  appId: "1:67906114245:web:6b06db08977407a836d6ac",
};

const Stack = createNativeStackNavigator();

export default function App() {
  const { user, checkAuth } = useFirebaseLogin();

  const [fight, setFight] = useState<boolean | null>(null);

  useEffect(() => {
    initializeApp(firebaseConfig);
    checkAuth();
  }, [checkAuth]);

  if (user == null) {
    return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="Connexion" component={Connection} />
          <Stack.Screen name="S'enregister" component={Register} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
  if (fight !== null) {
    return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="Deck"
            component={(props) => <Deck user={user} {...props} />}
            options={{
              headerLeft: () => (
                <TouchableOpacity onPress={() => setFight(null)}>
                  <MaterialIcons
                    name="keyboard-backspace"
                    size={24}
                    color="black"
                  />
                </TouchableOpacity>
              ),
            }}
          />
          <Stack.Screen
            name="Fight"
            component={Fight}
          />
          <Stack.Screen
            name="Win"
            component={(props) => <WinScreen setFight={setFight} {...props} />}
            options={{
              headerLeft: () => (
                <Text></Text>
              )
            }}
          />
          <Stack.Screen
            name="Loose"
            component={(props) => <LooseScreen setFight={setFight} {...props} />}
            options={{
              headerLeft: () => (
                <Text></Text>
              )
            }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
  return (
    <NavigationContainer>
      <BottomBar onClick={() => setFight(true)} user={user} />
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
