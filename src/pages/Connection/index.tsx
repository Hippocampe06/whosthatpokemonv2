import React, { useState, useEffect } from "react";
import { Image, ImageBackground, Modal, Text, TextInput, TouchableOpacity, View, LogBox } from "react-native";
import  useFirebaseLogin from "../../hooks/useFirebaseLogin"
import { Entypo } from '@expo/vector-icons'; 
import { Ionicons } from '@expo/vector-icons'; 
import { AntDesign } from '@expo/vector-icons';
import styles from "./style";

const Connection = ({ navigation }: any) => {
    // Login input
    const [email, onChangeEmail] = React.useState("");
    const [password, onChangePassword] = React.useState("");
    // Firebase
    const { connectEmailPassword, resetPassword, errorMessage, infoMessage } = useFirebaseLogin();
    // Reset password
    const [emailResetPassword, onChangeEmailResetPassword] = React.useState("");
    const [modalVisible, setModalVisible] = useState(false);

    useEffect(() => {
      LogBox.ignoreLogs(['']); // Ignore log notification by message
      LogBox.ignoreAllLogs();//Ignore all log notifications
    });

    useEffect(() => {
        errorMessage
        infoMessage
    }, [])

    return (
      <View style={styles.container}>
        <Modal
          animationType="slide"
          transparent={false}
          visible={modalVisible}
          onRequestClose={() => {
            setModalVisible(!modalVisible);
          }}
        >
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <Text>Email du compte</Text>
              <AntDesign
                style={styles.closeModal}
                onPress={() => setModalVisible(false)}
                name="closecircleo"
                size={24}
                color="black"
              />
              <View style={[styles.inputContainer, styles.marginTop20]}>
                <TextInput
                  style={styles.inputs}
                  onChangeText={onChangeEmailResetPassword}
                  value={emailResetPassword}
                  placeholder="Email"
                  keyboardType="email-address"
                  placeholderTextColor={"#d3d3d3"}
                />
                <Entypo
                  style={styles.inputIcon}
                  name="email"
                  size={24}
                  color="black"
                />
              </View>

              <TouchableOpacity
                style={[styles.resetButtonContainer, styles.loginButton]}
                onPress={() => [
                  resetPassword(emailResetPassword),
                  setModalVisible(false),
                ]}
              >
                <Text style={styles.loginText}>Réinitialiser</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>

        <Image
          style={styles.image}
          source={require("../../../assets/logo.png")}
        />
        <View style={styles.inputContainer}>
          <TextInput
            style={styles.inputs}
            onChangeText={onChangeEmail}
            value={email}
            placeholder="Email"
            autoCapitalize={"none"}
            keyboardType="email-address"
          />
          <Entypo
            style={styles.inputIcon}
            name="email"
            size={24}
            color="black"
          />
        </View>

        <View style={styles.inputContainer}>
          <TextInput
            style={styles.inputs}
            onChangeText={onChangePassword}
            value={password}
            placeholder="Mot de passe"
            secureTextEntry={true}
          />
          <Ionicons
            style={styles.inputIcon}
            name="key-outline"
            size={24}
            color="black"
          />
        </View>

        <TouchableOpacity
          style={styles.btnForgotPassword}
          onPress={() => setModalVisible(true)}
        >
          <Text style={styles.btnText}>mot de passe oublié ?</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={[styles.buttonContainer, styles.loginButton]}
          onPress={() => connectEmailPassword(email, password)}
        >
          <Text style={styles.loginText}>Se connecter</Text>
        </TouchableOpacity>

        {errorMessage !== "" ? (
          <Text style={styles.errorMessage}>{errorMessage}</Text>
        ) : null}

        {infoMessage !== "" ? (
          <Text style={styles.infoMessage}>{infoMessage}</Text>
        ) : null}

        <TouchableOpacity
          style={styles.buttonContainer}
          onPress={() => navigation.navigate("S'enregister")}
        >
          <Text style={styles.btnText}>S'enregistrer</Text>
        </TouchableOpacity>
      </View>
    );
}

export default Connection;