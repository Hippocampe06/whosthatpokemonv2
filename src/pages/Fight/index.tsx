import { useEffect, useState } from "react";
import { Animated, ImageBackground, StyleSheet, Text, View, Dimensions } from "react-native";
import { ItemDeck, ItemFight } from "../../components";
import useFight from "../../hooks/useFight";

import images from "../../../assets";

const Fight = ({ route, navigation }) => {
  // console.log(props.);
  const { pokemon, iaPokemon } = route.params;
  const { callMove, setPlayer, setIa, iaHp, playerHp, setIaHp, setPlayerHp } =
    useFight();

  useEffect(() => {
    setPlayer({
      type: "joueur",
      pokemon: pokemon,
    });
    setIa({
      type: "ia",
      pokemon: iaPokemon,
    });
    setIaHp(iaPokemon.hp);
    setPlayerHp(pokemon.hp);
  }, []);

  
  return (
    <View style={styles.view}>
      <ImageBackground
        source={images["backgroundFight"]}
        resizeMode="cover"
        style={styles.cardBackground}
      />
      <View style={styles.item}>
        <Text style={styles.whosthatpokemon}>Votre pokémon</Text>
        <ItemFight
          callMove={callMove}
          pokemon={pokemon}
          hp={playerHp}
          joueurType="joueur"
        />
      </View>

      <Animated.View style={styles.item}>
        <Text style={styles.whosthatpokemon}>
          Le pokémon de votre adversaire
        </Text>

        <ItemFight
          callMove={callMove}
          pokemon={iaPokemon}
          hp={iaHp}
          joueurType="ia"
        />
      </Animated.View>
    </View>
  );
};

const styles = StyleSheet.create({
  view: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-between",
    paddingVertical: 50,
  },
  item: {
    height: 250,
    padding: 15,
    borderWidth: 1,
    borderColor: "#000",
    margin: 10,
  },
  whosthatpokemon: {
    fontSize: 20,
    fontWeight: "bold",
    marginBottom: 20,
    marginTop: -10,
  },
  box: {
    marginTop: 32,
    borderRadius: 4,
    backgroundColor: "#61dafb",
  },
  cardBackground: {
    flex: 1,
    position: "absolute",
    top: 0,
    left: 0,
    height: Dimensions.get("screen").height,
    width: "100%",
    opacity: 0.2,
  },
});

export default Fight;
