export { default as About } from "./About";
export { default as Add } from "./Add";
export { default as Pokedex } from "./Pokedex";
export { default as Connection } from "./Connection";
export { default as Register } from "./Register";
export { default as Deck } from "./Deck";
export { default as Fight } from "./Fight";
export { default as WinScreen } from "./WinScreen";
export { default as LooseScreen } from "./LooseScreen";
