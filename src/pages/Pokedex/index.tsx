import React, { useEffect } from "react";
import {
  Image,
  ImageBackground,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  LogBox
} from "react-native";
import ItemPokedex from "../../components/ItemPokedex";
import useFirestore from "../../hooks/useFirestore";
import usePokeApi from "../../hooks/usePokeApi";
import images from "../../../assets";
import { FAB } from "react-native-paper";
import useStorage from "../../hooks/useStorage";
import { EmptyItemPokedex } from "../../components";

type PokedexProps = {
  user: any;
  onClick: () => void;
};

const Pokedex = (props: PokedexProps, navigation: any) => {
  const { cards, getCurrentUser, currentUserId, userCardType, userId } =
    useFirestore();

  const { getAllCardsFromUser, allUserImages, setAllUserImages } = useStorage();

  const { getPokemon } = usePokeApi();
  const { user, onClick } = props;

  useEffect(() => {
    LogBox.ignoreLogs(['']); // Ignore log notification by message
    LogBox.ignoreAllLogs();//Ignore all log notifications
  });

  useEffect(() => {
    if (userId != "") {
      getAllCardsFromUser(userId);
    }
  }, [userId]);

  useEffect(() => {
    if (allUserImages != []) {
      // let aa = allUserImages;
      // aa.sort((a, b) => {
      //   console.log(a, b);
      //   return Math.abs(a.dateUploaded.getTime() - b.dateUploaded.getTime());
      // });
      // setAllUserImages(aa);
    }
    console.log("allUserImages", allUserImages);
  }, [allUserImages]);

  getCurrentUser(user);

  return (
    <View style={styles.flex}>
      <ScrollView
        contentContainerStyle={{ flexGrow: 1, flexDirection: "column" }}
      >
        <ImageBackground
          style={styles.cardBackground}
          source={require("../../../assets/Background/meadow.jpg")}
          resizeMode="cover"
        />

        <View style={styles.container}>
          <Image style={styles.imgType} source={images[userCardType]} />
          <Text style={styles.title}>Pokedex de {currentUserId}</Text>
          <Image style={styles.imgType} source={images[userCardType]} />
        </View>

        {allUserImages.length == 0 ? (
          <EmptyItemPokedex userCardType={userCardType} nav={navigation} user={user} />
        ) : null}

        <View style={styles.flexCards}>
          {allUserImages.map((cardFromStorage, id) => {
            return <ItemPokedex cardFromStorage={cardFromStorage} key={id} />;
          })}
        </View>
      </ScrollView>
      <FAB style={styles.fab} icon="sword" onPress={onClick} />
    </View>
  );
};

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
  container: {
    justifyContent: "center",
    padding: 8,
    flexDirection: "row",
    alignItems: "center",
  },
  cardBackground: {
    flex: 1,
    position: "absolute",
    top: 0,
    left: 0,
    height: "100%",
    width: "100%",
    opacity: 0.3,
  },
  title: {
    textAlign: "center",
    fontSize: 20,
    marginTop: 15,
    marginBottom: 15,
    padding: 20,
    color: "black",
  },
  imgType: {
    width: 40,
    height: 40,
  },
  fab: {
    position: "absolute",
    bottom: 15,
    right: 15,
  },
  flexCards: {
    flexDirection: "column",
  },
  marginBottom: {
    marginBottom: 60
  }
});

export default Pokedex;
