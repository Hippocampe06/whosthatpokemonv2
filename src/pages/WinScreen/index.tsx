import { Image, ImageBackground, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import useEndScreen from "../../hooks/useEndScreen";
import { Entypo } from "@expo/vector-icons";
import { MaterialIcons } from "@expo/vector-icons";

import images from "../../../assets";

import { AntDesign } from "@expo/vector-icons";

import { Emitter } from "react-native-particles";

type WinScreenProps = {
  setFight: (p: null) => void;
};

const WinScreen = (props: WinScreenProps) => {
  const { replay } = useEndScreen();

  const { setFight } = props;

  return (
    <View style={styles.container}>
      <ImageBackground
        source={images["backgroundWinTest"]}
        resizeMode="cover"
        style={styles.cardBackground}
      />
      <View style={styles.particlesContainer}>
        <Emitter
          numberOfParticles={300}
          emissionRate={10}
          interval={100}
          particleLife={2000}
          autoStart={true}
          direction={-90}
          spread={150}
          gravity={0.2}
        >
          <AntDesign name="star" size={12} color="yellow" />
        </Emitter>
      </View>

      <Text style={styles.title}>Gagné ;)</Text>

      <View style={styles.imageContainer}>
        <Image source={images["win"]} style={styles.image} />
      </View>

      <View style={styles.flexContainer}>
        <TouchableOpacity style={styles.replay} onPress={() => replay()}>
          <MaterialIcons name="replay" size={24} color="black" />
          <Text style={styles.btnText}>Rejouer</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.quit} onPress={() => setFight(null)}>
          <Text style={styles.btnText}>Quitter les combats</Text>
          <Entypo name="cross" size={24} color="black" />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default WinScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  title: {
    fontSize: 25,
    textAlign: "center",
    marginTop: 30,
    marginBottom: 30,
  },
  btnText: {
    fontSize: 18,
  },
  replay: {
    flexDirection: "row",
    padding: 10,
    backgroundColor: "#00b5ec",
    shadowColor: "#808080",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.5,
    shadowRadius: 12.35,

    elevation: 1,
  },
  quit: {
    flexDirection: "row",
    padding: 10,
    backgroundColor: "#dc143c",
    shadowColor: "#808080",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.5,
    shadowRadius: 12.35,

    elevation: 1,
  },
  flexContainer: {
    flexDirection: "row",
    justifyContent: "space-evenly",
    maxHeight: 200,
    marginTop: 30,
  },
  imageContainer: {
    height: 300,
    justifyContent: "center",
    alignItems: "center",
  },
  image: {
    flex: 1,
    resizeMode: "contain",
    justifyContent: "center",
  },
  particlesContainer: {
    position: "absolute",
    left: "50%",
    top: 35,
    width: "100%",
  },
  cardBackground: {
    flex: 1,
    position: "absolute",
    top: 0,
    left: 0,
    height: "100%",
    width: "100%",
    opacity: 0.5
  },
});
