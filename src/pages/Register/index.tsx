import React, { useEffect } from 'react';
import { Image, Platform, Text, TextInput, TouchableOpacity, View, LogBox } from 'react-native';
import stylesConnection from "../Connection/style";
import stylesRegister from "./style";
import { Entypo } from '@expo/vector-icons'; 
import { Ionicons } from '@expo/vector-icons'; 
import { FontAwesome5 } from '@expo/vector-icons';
import  useFirebaseLogin from "../../hooks/useFirebaseLogin"
import {Picker, PickerIOS} from '@react-native-picker/picker';

const Register = () => {
    const [email, onChangeEmail] = React.useState("");
    const [password, onChangePassword] = React.useState("");
    const [userName, onChangeUserName] = React.useState("");
    const {createUser, errorMessage } = useFirebaseLogin();
    const [selectedType, setSelectedType] = React.useState("");

    useEffect(() => {
        LogBox.ignoreLogs(['']); // Ignore log notification by message
        LogBox.ignoreAllLogs();//Ignore all log notifications
      });

    useEffect(() => {
        errorMessage
    }, [])

    const type = [
        {
            id: 1,
            label: 'Feu',
            value: 'cardTypeFire'
        },
        {
            id: 2,
            label: 'Eau',
            value: 'cardTypeWater'
        },
        {
            id: 3,
            label: 'Plante',
            value: 'cardTypeGrass'
        },
        {
            id: 3,
            label: 'Obscur',
            value: 'cardTypeDark'
        },
        {
            id: 4,
            label: 'Dragon',
            value: 'cardTypeDragon'
        },
        {
            id: 5,
            label: 'Electrique',
            value: 'cardTypeElectric'
        },
        {
            id: 6,
            label: 'Fée',
            value: 'cardTypeFairy'
        },
        {
            id: 7,
            label: 'Combat',
            value: 'cardTypeFighting'
        },
        {
            id: 8,
            label: 'Psychique',
            value: 'cardTypePsychic'
        },
        {
            id: 9,
            label: 'Acier',
            value: 'cardTypeSteel'
        },
        {
            id: 10,
            label: 'Normal',
            value: 'cardTypeNormal'
        },
    ]; 

    return (
        <View style={stylesConnection.container}>

        <Image
             style={stylesConnection.image}
             source={require("../../../assets/logo.png")}
         />
         <View style={stylesConnection.inputContainer}>
             <TextInput
                 style={stylesConnection.inputs}
                 onChangeText={onChangeEmail}
                 value={email}
                 placeholder="Email"
                 keyboardType="email-address"
             />
             <Entypo style={stylesConnection.inputIcon} name="email" size={24} color="black" />
         </View>

         <View style={stylesConnection.inputContainer}>
             <TextInput
                 style={stylesConnection.inputs}
                 onChangeText={onChangePassword}
                 value={password}
                 placeholder="Mot de passe"
                 secureTextEntry={true}
             />
             <Ionicons style={stylesConnection.inputIcon} name="key-outline" size={24} color="black" />
         </View>

         <View style={stylesConnection.inputContainer}>
             <TextInput
                 style={stylesConnection.inputs}
                 onChangeText={onChangeUserName}
                 value={userName}
                 placeholder="Pseudo"
             />
             <FontAwesome5 style={stylesConnection.inputIcon} name="user-ninja" size={24} color="black" />
         </View>

         <Text>Votre type de pokémon préféré</Text>

         {/* Cette immondie là est la création de Christian :) */}
         {Platform.OS === "ios" ? (
         <View style={[stylesConnection.inputContainer, stylesRegister.marginTop]}>
                <Picker style={stylesRegister.cell}
                    selectedValue={selectedType}
                    onValueChange={(itemValue) =>
                        setSelectedType(itemValue.toString())
                    }>
                    {type?.map((element) => {
                        return(
                            <Picker.Item key={"type_"+element.id} label={element.label} value={element.value} />
                        )
                    })}
                </Picker>
        </View>
         ) : 
         <View style={[stylesConnection.inputContainer]}>
                <Picker style={stylesRegister.cell}
                    selectedValue={selectedType}
                    onValueChange={(itemValue) =>
                        setSelectedType(itemValue)
                    }>
                     {type?.map((element) => {
                        return(
                            <PickerIOS.Item key={"type_"+element.id} label={element.label} value={element.value} />
                        )
                    })}
                </Picker>
         </View>
         }
        {/* Jusqu'ici ! */}


         
         <TouchableOpacity style={[stylesConnection.buttonContainer, stylesConnection.loginButton]} onPress={() => createUser(email, password, userName, selectedType)}>
             <Text style={stylesConnection.loginText}>S'enregistrer</Text>
         </TouchableOpacity>

         {errorMessage !== "" ? (
                <Text style={stylesConnection.errorMessage} >{errorMessage}</Text> 
        ): null }
     
     </View>
    )

}

export default Register;