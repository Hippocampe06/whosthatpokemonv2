import { useEffect, useState } from "react";
import {
  Dimensions,
  ImageBackground,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { ItemDeck } from "../../components";
import { Ionicons } from "@expo/vector-icons";
import { MaterialCommunityIcons } from "@expo/vector-icons";

import SnackBar from "react-native-snackbar-component";
import useDeck from "../../hooks/useDeck";
import useFirestore from "../../hooks/useFirestore";

import images from "../../../assets";
import useEndScreen from "../../hooks/useEndScreen";

type DeckProps = {
  user: any;
};

const Deck = (props: DeckProps, {route, navigation}:  any) => {
  const { user } = props;

  const { launchFight } = useEndScreen();

  const { getCurrentUser, userCardType } = useFirestore();

  const { pokemons } = useDeck();

  const [selected, setSelected] = useState<Array<boolean>>(
    Array(pokemons?.length).fill(false)
  );
  const [selectedPokemons, setSelectedPokemons] = useState<
    Array<any> | Array<Pokemon>
  >([]);

  const [iaPokemon, setIaPokemon] = useState<any | Pokemon>();
  const [ready, setReady] = useState<boolean>(false);
  const [error, setError] = useState<boolean>(false);

  const selectItem = (id: number) => {
    let allCurrentlySelected = [...selected];
    if (!allCurrentlySelected[id]) {
      if (selectedPokemons.length > 0) {
        setError(true);
      } else {
        allCurrentlySelected[id] = true;
        addPokemonToDeck(id);
        setSelected(allCurrentlySelected);
      }
    } else {
      removePokemonFromDeck();
    }
  };

  useEffect(() => {
    getCurrentUser(user);
  }, []);

  const addPokemonToDeck = (id: number) => {
    setSelectedPokemons([pokemons[id]]);
  };

  const removePokemonFromDeck = () => {
    setSelectedPokemons([]);
    setSelected(Array(pokemons?.length).fill(false));
  };

  const rdIntBetween = (min: number, max: number) => {
    // min and max included
    return Math.floor(Math.random() * (max - min + 1) + min);
  };

  const goFight = () => {
    launchFight(selectedPokemons[0], iaPokemon);
  };

  useEffect(() => {
    setReady(selectedPokemons.length > 0 ? true : false);
    const idTakenByPlayer = selected.indexOf(true);

    let rdPokemonId = rdIntBetween(0, pokemons.length);

    while (rdPokemonId == idTakenByPlayer) {
      rdPokemonId = rdIntBetween(0, pokemons.length);
    }
    console.log("rdPokemonId", rdPokemonId);
    console.log("idTakenByPlayer", idTakenByPlayer);
    setIaPokemon(pokemons[rdPokemonId]);
  }, [selectedPokemons]);

  const fixTypeBackground = (s: string) => {
    return s.replace("Type", "Background");
  };

  return (
    <View style={styles.parent}>
      <ImageBackground
        source={images[fixTypeBackground(userCardType)]}
        resizeMode="cover"
        style={styles.cardBackground}
      />

      <Text style={styles.title}>Choisissez votre pokémon :</Text>
      <ScrollView style={styles.scroll}>
        <View style={styles.flex}>
          {pokemons?.map((pokemon, id) => {
            return (
              <TouchableOpacity
                style={styles.item}
                key={`poke-${id}`}
                onPress={() => selectItem(id)}
              >
                {selected[id] && (
                  <Ionicons
                    style={styles.checked}
                    name="checkmark-circle"
                    size={24}
                    color="black"
                  />
                )}

                <ItemDeck pokemon={pokemon} />
              </TouchableOpacity>
            );
          })}
        </View>
      </ScrollView>

      <TouchableOpacity
        style={!ready ? styles.disabled : styles.goFight}
        disabled={!ready}
        onPress={() => {
          goFight();
        }}
      >
        {!ready && (
          <View style={styles.flexButton}>
            <Text style={styles.goFightText}>Choisissez un pokémon</Text>
            <MaterialCommunityIcons
              name="alert-outline"
              size={24}
              color="black"
            />
          </View>
        )}
        {ready && (
          <View style={styles.flexButton}>
            <Text style={styles.goFightText}>Lancer la partie</Text>
            <MaterialCommunityIcons name="sword" size={24} color="black" />
          </View>
        )}
      </TouchableOpacity>

      <SnackBar
        visible={error}
        textMessage="Vous devez choisir 1 seul pokémon !"
        actionHandler={() => {
          setError(false);
        }}
        actionText="Compris"
      />
    </View>
  );
};
const styles = StyleSheet.create({
  flex: {
    flexWrap: "wrap",
    flexDirection: "row",
  },
  scroll: {
    maxHeight: "75%",
  },
  parent: {
    paddingHorizontal: 10,
  },
  title: {
    fontSize: 25,
    textAlign: "center",
    marginTop: 30,
    marginBottom: 30,
  },
  item: {
    flex: 1,
    flexBasis: "40%",
    height: 150,
    padding: 15,
    borderWidth: 1,
    borderColor: "#000",
    margin: 10,
  },
  checked: {
    position: "absolute",
    top: 10,
    right: 10,
  },
  goFight: {
    width: "100%",
    height: 50,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#e3e3e3",
    marginBottom: 50,
    marginTop: 15,
  },
  goFightText: {
    color: "black",
    fontSize: 16,
    marginRight: 5,
  },
  disabled: {
    width: "100%",
    height: 50,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "red",
    marginBottom: 50,
    marginTop: 15,
  },
  flexButton: {
    flexDirection: "row",
  },
  cardBackground: {
    flex: 1,
    position: "absolute",
    top: 0,
    left: 0,
    height: "100%",
    width: Dimensions.get("screen").width,
    opacity: 0.3,
  },
});

export default Deck;
