import { useEffect, useState } from "react";
import { ScrollView, StyleSheet, Text, TouchableOpacity, View } from "react-native"
import { ItemDeck } from "../../components";
import { Ionicons } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons'; 

import SnackBar from 'react-native-snackbar-component';
import useFight from "../../hooks/useDeck";


const Deck = ({ navigation }: any) => {

    const { pokemons } = useFight();

    const [selected, setSelected] = useState<Array<boolean>>(
        Array(pokemons?.length).fill(false)
    )

    const [selectedPokemons, setSelectedPokemons] = useState<Array<any> | Array<Pokemon>>([]);
    const [ready, setReady] = useState<boolean>(false);

    const [error, setError] = useState<boolean>(false);

    const countOccurrences = (arr: Array<any>, val: any) => arr.reduce((a, v) => (v === val ? a + 1 : a), 0);

    const selectItem = (id: number) => {
        // let allCurrentlySelected = [...selected];
        // if (!allCurrentlySelected[id]) {
        //     // More than 1 pokemons to be selected --> Alert that user can't do that
        //     if (countOccurrences(selected, true) > 0) {
        //         setError(true);
        //     } else {
        //         addPokemonToDeck(id);
        //         allCurrentlySelected[id] = true
        //     }
        // } else {
        //     removePokemonFromDeck(id);
        //     allCurrentlySelected[id] = false
        // }
        // setSelected(allCurrentlySelected);

        let allCurrentlySelected = [...selected];
        if (!allCurrentlySelected[id]) {
            if (selectedPokemons.length > 0) {
                setError(true);
            } else {
                allCurrentlySelected[id] = true;
                addPokemonToDeck(id);
                setSelected(allCurrentlySelected);
            }
        } else {
            removePokemonFromDeck();
        }

        
    }

    const addPokemonToDeck = (id: number) => {
        // let newPokemons = [...selectedPokemons];
        // newPokemons.push(pokemons[id]);
        // setSelectedPokemons(newPokemons);
        setSelectedPokemons([pokemons[id]]);
    }

    const removePokemonFromDeck = () => {
        // const pokemonId = pokemons[id].id;
        // const match = (pokemon: Pokemon) => pokemon.id == pokemonId;
        // const idToRemove = selectedPokemons.findIndex(match);
        // let newPokemons = [...selectedPokemons];
        // newPokemons.splice(idToRemove, 1);
        // setSelectedPokemons(newPokemons);
        setSelectedPokemons([]);
    }

    const goFight = () => {
        navigation.navigate("Fight", {
            pokemons: selectedPokemons
        });
    }


    useEffect(() => {
        setReady(selectedPokemons.length > 0 ? true : false);
    }, [selectedPokemons])



    return (
        <ScrollView
            style={styles.parent}>
            <Text>Choisissez votre deck de 2 pokémons : </Text>
            <View
                style={styles.flex}>
                {pokemons?.map((pokemon, id) => {
                    return (
                        <TouchableOpacity
                            style={styles.item}
                            key={`poke-${id}`}
                            onPress={() => selectItem(id)}>

                            {selected[id] && (
                                <Ionicons
                                    style={styles.checked}
                                    name="checkmark-circle"
                                    size={24}
                                    color="black" />
                            )}

                            <ItemDeck
                                pokemon={pokemon} />

                        </TouchableOpacity>
                    )
                })}
            </View>

            

            <TouchableOpacity
                style={!ready ? styles.disabled : styles.goFight}
                disabled={!ready}
                onPress={() => {goFight()}}>
                <Text
                    style={styles.goFightText}>
                    Aller se taper sur la gueule
                </Text>
                
                {!ready && (
                    <MaterialCommunityIcons name="alert-outline" size={24} color="black" />
                )}
                {ready && (
                    <MaterialCommunityIcons name="sword" size={24} color="black" />
                )}


            </TouchableOpacity>

            <SnackBar 
                visible={error} 
                textMessage="Vous devez choisir 1 seul pokémon !" 
                actionHandler={()=>{setError(false)}} 
                actionText="Compris"/>
                
        </ScrollView>
    )
}
const styles = StyleSheet.create({
    flex: {
        flexWrap: 'wrap',
        flexDirection: 'row'
    },
    parent: {
        paddingHorizontal: 10
    },
    item: {
        flex: 1,
        flexBasis: "40%",
        height: 150,
        padding: 15,
        borderWidth: 1,
        borderColor: "#000",
        margin: 10
    },
    checked: {
        position: "absolute",
        top: 10,
        right: 10
    },
    goFight: {
        width: "100%",
        height: 50,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "row",
        backgroundColor: "#e3e3e3",
        marginBottom: 15
    },
    goFightText: {
        color: "black",
        fontSize: 16,
        marginRight: 5
    },
    disabled: {
        width: "100%",
        height: 50,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "row",
        backgroundColor: "red",
        marginBottom: 15
    }
})

export default Deck;