import React, { useState, useEffect, useRef } from "react";
import {
  StyleSheet,
  ScrollView,
  Text,
  View,
  ImageBackground,
  TextInput,
  Dimensions,
  TouchableOpacity,
  Pressable,
  Image,
  Modal,
  Platform,
  LogBox
} from "react-native";
import { Picker, PickerIOS } from "@react-native-picker/picker";
import * as ImagePicker from "expo-image-picker";
import { Camera } from "expo-camera";
import { AntDesign } from "@expo/vector-icons";
import { MaterialIcons } from "@expo/vector-icons";
import { Ionicons } from "@expo/vector-icons";
import assetImages from "../../../assets";
import ViewShot from "react-native-view-shot";
import * as Permissions from "expo-permissions";
import * as MediaLibrary from "expo-media-library";
import useStorage from "../../hooks/useStorage";
import * as Progress from "react-native-progress";
import useFirestore from "../../hooks/useFirestore";

type AddProps = {
  user: any;
};

const Add = (props: AddProps) => {
  // Set card dimensions
  const [imgWidth, setImgWidth] = useState<number>(0);
  const [imgHeight, setImgHeight] = useState<number>(0);
  const [cardTypeComponentHeight, setCardTypeComponentHeight] =
    useState<number>(0);

  // Manage image picker modal
  const [modalVisible, setModalVisible] = useState(false);

  // Get Camera permissions
  const [hasPermission, setHasPermission] = useState<boolean>(false);
  const [type, setType] = useState(Camera.Constants.Type.back);
  // Check for camera permission display
  const [camera, setCamera] = useState<boolean | null>(null);
  // Used to take image with the camera
  const [cameraTake, setCameraTake] = useState<any>();

  const [isBorderActive, setIsBorderActive] = useState<boolean>(true);

  // Get card inputs
  const [name, setName] = useState<string>("");
  const [health, setHealth] = useState<string>("");
  const [attackTitle1, setAttackTitle1] = useState<string>("");
  const [attackDesc1, setAttackDesc1] = useState<string>("");
  const [attackDamage1, setAttackDamage1] = useState<string>("");
  const [attackTitle2, setAttackTitle2] = useState<string>("");
  const [attackDesc2, setAttackDesc2] = useState<string>("");
  const [attackDamage2, setAttackDamage2] = useState<string>("");

  const [isPickerOpen, setIsPickerOpen] = useState<boolean>(false);

  // Get card custom inputs
  const [image, setImage] = useState<string>("");
  const [imageTopPlacement, setImageTopPlacement] =
    useState<string | number>("");
  // A noter que c'est Ludovic le responsable
  // de cette immondie :)
  const [cardType, setCardType] = useState<string>(
    "cardNormal,cardBackgroundNormal"
  );

  const [cardBackground, setCardBackground] = useState<string>("");
  const [cardImage, setCardImage] = useState<string>("");

  const { getCurrentUser, createCard, userId } = useFirestore();
  const { user } = props;

  const calculateRelativeHeight = (windowHeight: number) => {
    // Get the card ratio from the original card size
    const ratio = Math.ceil(418 / 590);
    // Set the newImage height by multiplying card ratio by screen Height
    // Add to the screen height a diminution ratio for small mobiles
    // (could be variable taking [biggest size divided by [smallest size multiply by 2]])
    // Remove the sum of top bar + bottom bar
    setImgHeight(ratio * (windowHeight / 1.35) - (64 + 48));
  };

  useEffect(() => {
    LogBox.ignoreLogs(['']); // Ignore log notification by message
    LogBox.ignoreAllLogs();//Ignore all log notifications
  });

  useEffect(() => {
    // When the height is set recalculate the width keeping card ratio with the new height
    setImgWidth(Math.ceil(418 * imgHeight) / 590);
    // Recalculate card type component drop down because min height as percentage don't works
    // set the height as 6% of the card height (other properties are working with percentage
    // so it will be responsive)
    console.log("Size ", Math.ceil(imgHeight * 0.06));
    setCardTypeComponentHeight(Math.ceil(imgHeight * 0.06));
  }, [imgHeight]);

  useEffect(() => {
    console.log("Card Type Changed");
    console.log("Card Type : ", cardType);
    const ct = cardType.split(",");
    setCardImage(ct[0]);
    setCardBackground(ct[1]);
  }, [cardType]);

  useEffect(() => {
    // On view load, calculate card dimensions
    calculateRelativeHeight(Dimensions.get("window").height);

    // Get permission to use camera component
    (async () => {
      const { status } = await Camera.requestCameraPermissionsAsync();
      setHasPermission(status === "granted");
    })();

    getCurrentUser(user);
    console.log("USER : ", user);
    console.log("USER : ", getCurrentUser(user));
  }, []);

  // Open image by gallery method
  const imageGallery = async () => {
    console.log("Gallery Pressed");

    // No permissions request is necessary for launching the image library
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 0.5,
    });

    // If a picture is selected
    if (!result.cancelled) {
      console.log("Gallery image uri : ", result.uri);
      // set image with the uri to update display
      setImageTopPlacement("10%");
      setImage(result.uri);
    }
  };

  // Check camera permissions before display
  const imageCamera = async () => {
    console.log("Camera Pressed");
    console.log("Camera Permission : ", hasPermission);

    setCamera(hasPermission == null ? false : hasPermission);
  };

  // Get camera event
  const takePicture = () => {
    if (cameraTake) {
      // Call onPictureSaved to display image
      cameraTake.takePictureAsync({ onPictureSaved: onPictureSaved });
    }
  };

  // Display image taken on the card
  const onPictureSaved = (photo: any) => {
    console.log("Camera saved");

    if (!photo.cancelled) {
      console.log("Camera image uri : ", photo.uri);
      // set image with the uri to update display
      setImageTopPlacement("5.5%");
      setImage(photo.uri);
    }
    // Hide camera after image upload to the card
    setCamera(false);
  };

  const viewShotRef = useRef<any>();

  async function captureViewShot() {
    if (viewShotRef.current != undefined) {
      const imageURI = await viewShotRef.current.capture();
      // Download picture in the gallery
      handleDownload(imageURI);
    }
  }

  // Manage image to download to gallery
  async function handleDownload(imageURI: string) {
    // Ask for permission to use gallery (deprecated)
    const perm = await Permissions.askAsync(Permissions.MEDIA_LIBRARY);
    if (perm.status != "granted") {
      return;
    }
    // Save image to gallery
    try {
      const asset = await MediaLibrary.createAssetAsync(imageURI);
      const album = await MediaLibrary.getAlbumAsync("Download");
      if (album == null) {
        await MediaLibrary.createAlbumAsync("Download", asset, false);
      } else {
        await MediaLibrary.addAssetsToAlbumAsync([asset], album, false);
      }
    } catch (e) {
      console.log("Error : ", e);
    }
  }

  /** Storage Firebase **/
  const { uploadImageAsync, progress, isUpload } = useStorage();

  async function getViewShot() {
    if (viewShotRef.current != undefined) {
      getCurrentUser(user);
      const imageURI = await viewShotRef.current.capture();
      // Save image to firestore
      uploadImageAsync(imageURI, userId);
    }
  }

  return (
    <ScrollView
      style={styles.container}
      contentContainerStyle={{ flexGrow: 1, flexDirection: "column" }}
    >
      {/* View background */}
      <ImageBackground
        source={assetImages[cardBackground]}
        resizeMode="cover"
        style={[styles.cardBackground, { paddingTop: 50 }]}
      >
        {/* Viewshot allows to capture content as image */}
        <ViewShot
          ref={viewShotRef}
          style={{ flex: 1 }}
          options={{ format: "png", quality: 0.6 }}
        >
          {/* Card structure */}
          <ImageBackground
            source={assetImages[cardImage]}
            resizeMode="cover"
            style={[styles.card, { width: imgWidth, height: imgHeight }]}
          >
            {/* Input Card Name */}
            <TextInput
              style={[
                styles.cardInput,
                {
                  top: "4%",
                  left: "25%",
                  width: "40%",
                  height: "4.5%",
                },
                !isBorderActive ? styles.noBorder : null,
              ]}
              maxLength={15}
              multiline={false}
              value={name}
              onChangeText={setName}
              editable
            />

            {/* Input Card Health */}
            <TextInput
              style={[
                styles.cardInput,
                {
                  top: "4%",
                  left: "73%",
                  width: "8%",
                  height: "4.5%",
                },
                !isBorderActive ? styles.noBorder : null,
              ]}
              maxLength={3}
              keyboardType="numeric"
              multiline={false}
              value={health}
              onChangeText={setHealth}
              editable
            />

            {/* Label Card Health (HP) */}
            <Text
              style={[
                styles.cardHealth,
                {
                  top: "4%",
                  left: "81%",
                  width: "6%",
                  height: "4.5%",
                },
                !isBorderActive ? styles.noBorder : null,
              ]}
            >
              HP
            </Text>

            {/* Input Card Type */}

            {Platform.OS === "ios" ? (
              <View
                style={{
                  zIndex: isPickerOpen ? 3 : 0
                }}>
                <TouchableOpacity
                  onPress={() => setIsPickerOpen(!isPickerOpen)}
                  style={{
                    top: "3%",
                    left: "88%",
                    minHeight: cardTypeComponentHeight + 20,
                    width: "8%",
                    display: "flex"
                  }}
                ></TouchableOpacity>
                <PickerIOS
                  testID="basic-picker"
                  selectedValue={cardType}
                  onValueChange={(cardType, cardIndex) => setCardType(cardType)}
                  accessibilityLabel="Basic Picker Accessibility Label"
                  style={[
                    styles.cardInput,
                    styles.cardTypeIos,
                    {
                      top: 50,
                      right: "0%",
                      minHeight: cardTypeComponentHeight,
                      width: "60%",
                      display: isPickerOpen ? "flex" : "none"
                    },
                    !isBorderActive ? styles.dispNone : null,
                  ]}
                >
                  <PickerIOS.Item
                    label="Type Normal"
                    value="cardNormal,cardBackgroundNormal"
                    key="cardNormal,cardBackgroundNormal"
                  />
                  <PickerIOS.Item
                    label="Type Obscur"
                    value="cardDark,cardBackgroundDark"
                  />
                  <PickerIOS.Item
                    label="Type Dragon"
                    value="cardDragon,cardBackgroundDragon"
                  />
                  <PickerIOS.Item
                    label="Type Electrique"
                    value="cardElectric,cardBackgroundElectric"
                  />
                  <PickerIOS.Item
                    label="Type Fée"
                    value="cardFairy,cardBackgroundFairy"
                  />
                  <PickerIOS.Item
                    label="Type Combat"
                    value="cardFighting,cardBackgroundFighting"
                  />
                  <PickerIOS.Item
                    label="Type Feu"
                    value="cardFire,cardBackgroundFire"
                  />
                  <PickerIOS.Item
                    label="Type Plante"
                    value="cardGrass,cardBackgroundGrass"
                  />
                  <PickerIOS.Item
                    label="Type Psychique"
                    value="cardPsychic,cardBackgroundPsychic"
                  />
                  <PickerIOS.Item
                    label="Type Acier"
                    value="cardSteel,cardBackgroundSteel"
                  />
                  <PickerIOS.Item
                    label="Type Eau"
                    value="cardWater,cardBackgroundWater"
                  />
                </PickerIOS>
              </View>
            ) : (
              <Picker
                testID="basic-picker"
                selectedValue={cardType}
                onValueChange={(cardType) => setCardType(cardType)}
                accessibilityLabel="Basic Picker Accessibility Label"
                style={[
                  styles.cardInput,
                  styles.cardType,
                  {
                    top: "3%",
                    left: "88%",
                    minHeight: cardTypeComponentHeight,
                    width: "8%",
                  },
                ]}
              >
                <Picker.Item
                  label="Type Normal"
                  value="cardNormal,cardBackgroundNormal"
                />
                <Picker.Item
                  label="Type Obscur"
                  value="cardDark,cardBackgroundDark"
                />
                <Picker.Item
                  label="Type Dragon"
                  value="cardDragon,cardBackgroundDragon"
                />
                <Picker.Item
                  label="Type Electrique"
                  value="cardElectric,cardBackgroundElectric"
                />
                <Picker.Item
                  label="Type Fée"
                  value="cardFairy,cardBackgroundFairy"
                />
                <Picker.Item
                  label="Type Combat"
                  value="cardFighting,cardBackgroundFighting"
                />
                <Picker.Item
                  label="Type Feu"
                  value="cardFire,cardBackgroundFire"
                />
                <Picker.Item
                  label="Type Plante"
                  value="cardGrass,cardBackgroundGrass"
                />
                <Picker.Item
                  label="Type Psychique"
                  value="cardPsychic,cardBackgroundPsychic"
                />
                <Picker.Item
                  label="Type Acier"
                  value="cardSteel,cardBackgroundSteel"
                />
                <Picker.Item
                  label="Type Eau"
                  value="cardWater,cardBackgroundWater"
                />
              </Picker>
            )}

            {/* Input Card Image if there is no image can be selected */}
            <Pressable
              style={[
                styles.cardInput,
                styles.cardImage,
                {
                  top: "10%",
                  left: "8%",
                  width: "85%",
                  height: "40%",
                },
                !isBorderActive ? styles.noBorder : null,
              ]}
              onPress={() => setModalVisible(true)}
            />

            {/* Set "Getting image modal" */}
            <Modal
              animationType="slide"
              transparent={false}
              visible={modalVisible}
              onRequestClose={() => {
                setModalVisible(!modalVisible);
              }}
            >
              <View style={styles.centeredView}>
                <View style={styles.modalView}>
                  <Text style={[{ marginTop: 20 }]}>
                    Choisir la méthode de récupération d'image
                  </Text>

                  {/* Close Modal Button */}
                  <AntDesign
                    style={styles.closeModal}
                    onPress={() => setModalVisible(false)}
                    name="closecircleo"
                    size={24}
                    color="black"
                  />

                  {/* Hide gallery image if camera is selected to display camera permission message if necessary */}
                  <TouchableOpacity
                    style={[styles.modalOptionButton, { marginTop: 30 }]}
                    onPress={() => [
                      imageCamera(),
                      setImage(""),
                      setModalVisible(false),
                    ]}
                  >
                    <Text style={styles.modalOption}>Caméra</Text>
                  </TouchableOpacity>

                  <TouchableOpacity
                    style={[styles.modalOptionButton]}
                    onPress={() => [imageGallery(), setModalVisible(false)]}
                  >
                    <Text style={styles.modalOption}>Gallerie</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </Modal>

            {/* Controls camera permissions before display */}
            {camera === true ? (
              // Add camera container
              <View
                style={{
                  position: "relative",
                  top: 0,
                  left: 0,
                  height: "100%",
                }}
              >
                {/* Set the camera component sized on card image to match */}
                {/* Add ref event to take camera picture */}
                <Camera
                  type={type}
                  ref={(ref) => {
                    setCameraTake(ref);
                  }}
                  style={[
                    {
                      top: "10%",
                      left: "8%",
                      width: "85%",
                      height: "40%",
                    },
                  ]}
                >
                  {/* Set camera option buttons */}
                  <View style={{ height: "100%" }}>
                    {/* Close camera button */}
                    <TouchableOpacity
                      style={{ position: "absolute", right: 0, margin: 10 }}
                      onPress={() => {
                        setCamera(false);
                      }}
                    >
                      <Text>
                        {" "}
                        <AntDesign
                          name="closecircleo"
                          size={48}
                          color="#FAE16D"
                        />{" "}
                      </Text>
                    </TouchableOpacity>

                    {/* Flip button */}
                    <TouchableOpacity
                      style={{ position: "absolute", top: 0, margin: 10 }}
                      onPress={() => {
                        setType(
                          type === Camera.Constants.Type.back
                            ? Camera.Constants.Type.front
                            : Camera.Constants.Type.back
                        );
                      }}
                    >
                      <Text>
                        <MaterialIcons
                          name="flip-camera-ios"
                          size={48}
                          color="#FAE16D"
                        />
                      </Text>
                    </TouchableOpacity>

                    {/* Take picture button */}
                    <TouchableOpacity
                      style={{ position: "absolute", bottom: 0, margin: 10 }}
                      onPress={(ref) => {
                        takePicture();
                      }}
                    >
                      <Text>
                        {" "}
                        <Ionicons name="ios-camera" size={48} color="#FAE16D" />
                      </Text>
                    </TouchableOpacity>
                  </View>
                </Camera>
              </View>
            ) : (
              true
            )}

            {/* Display an error message if permission is denied */}
            {camera === false ? (
              <TouchableOpacity
                onPress={() => setModalVisible(true)}
                style={{
                  position: "absolute",
                  top: "10%",
                  left: "8%",
                  width: "85%",
                  height: "40%",
                }}
              >
                <Text style={{ padding: "5%" }}>
                  L'accès à la caméra à été refusé... Vous devriez aller les
                  activer dans les paramètres de l'application
                </Text>
              </TouchableOpacity>
            ) : (
              false
            )}

            {/* Empty text on first connection */}
            {camera === null ? <Text></Text> : null}

            {/* Display image if selected and display "getting image modal"*/}
            {image !== "" ? (
              <TouchableOpacity
                onPress={() => setModalVisible(true)}
                style={[
                  {
                    top: imageTopPlacement,
                    left: "8%",
                    width: "85%",
                    height: "40%",
                  },
                ]}
              >
                <Image
                  source={{ uri: image }}
                  resizeMode="cover"
                  style={[
                    {
                      top: 0,
                      left: 0,
                      width: "100%",
                      height: "100%"
                    },
                  ]}
                />
              </TouchableOpacity>
            ) : undefined}

            {/* Input Card Attack Title 1 */}
            <TextInput
              style={[
                styles.cardInput,
                {
                  top: "55%",
                  left: "8%",
                  width: "60%",
                  height: "4.5%",
                },
                !isBorderActive ? styles.noBorder : null,
              ]}
              maxLength={25}
              multiline={false}
              value={attackTitle1}
              onChangeText={setAttackTitle1}
              editable
            />

            {/* Input Card Attack Damage 1 */}
            <TextInput
              style={[
                styles.cardInput,
                {
                  top: "55%",
                  left: "82%",
                  width: "9%",
                  height: "4.5%",
                },
                !isBorderActive ? styles.noBorder : null,
              ]}
              maxLength={3}
              keyboardType="numeric"
              multiline={false}
              value={attackDamage1}
              onChangeText={setAttackDamage1}
              editable
            />

            {/* Input Card Attack Description 1 */}
            <TextInput
              style={[
                styles.cardInput,
                styles.cardDescription,
                {
                  top: "60%",
                  left: "8%",
                  width: "60%",
                  height: "8%",
                },
                !isBorderActive ? styles.noBorder : null,
              ]}
              numberOfLines={3}
              multiline={true}
              value={attackDesc1}
              onChangeText={setAttackDesc1}
              editable
            />

            {/* Input Card Attack Title 2 */}
            <TextInput
              style={[
                styles.cardInput,
                {
                  top: "69%",
                  left: "8%",
                  width: "60%",
                  height: "4.5%",
                },
                !isBorderActive ? styles.noBorder : null,
              ]}
              maxLength={25}
              multiline={false}
              value={attackTitle2}
              onChangeText={setAttackTitle2}
              editable
            />

            {/* Input Card Attack Damage 2 */}
            <TextInput
              style={[
                styles.cardInput,
                {
                  top: "69%",
                  left: "82%",
                  width: "9%",
                  height: "4.5%",
                },
                !isBorderActive ? styles.noBorder : null,
              ]}
              maxLength={3}
              keyboardType="numeric"
              multiline={false}
              value={attackDamage2}
              onChangeText={setAttackDamage2}
              editable
            />

            {/* Input Card Attack Description 2 */}
            <TextInput
              style={[
                styles.cardInput,
                styles.cardDescription,
                {
                  top: "74%",
                  left: "8%",
                  width: "60%",
                  height: "8%",
                },
                !isBorderActive ? styles.noBorder : null,
              ]}
              numberOfLines={3}
              multiline={true}
              value={attackDesc2}
              onChangeText={setAttackDesc2}
              editable
            />
          </ImageBackground>
        </ViewShot>

        {/* Get card fields in console */}
        {/* <TouchableOpacity
          style={[styles.buttonContainer, styles.button, styles.marginTop]}
          onPress={() =>
            createCard(
              name,
              health,
              cardType,
              attackTitle1,
              attackTitle2,
              attackDamage1,
              attackDamage2,
              attackDesc1,
              attackDesc2
            )
          }
        >
          <Text style={styles.btnText}>Save</Text>
        </TouchableOpacity> */}

        {/* Download card to gallery */}
        <TouchableOpacity
          style={[styles.buttonContainer, styles.button, styles.marginTop]}
          onPress={() => {
            setIsBorderActive(false);
            captureViewShot().then(() => {
              setIsBorderActive(true);
            });
          }}
        >
          <Text style={styles.btnText}>Enregistrer en galerie</Text>
        </TouchableOpacity>

        {/* Download card to storage firebase */}
        {isUpload ? (
          <View style={styles.progressBarContainer}>
            <Progress.Bar progress={progress} width={300} />
          </View>
        ) : (
          <TouchableOpacity
            style={[styles.buttonContainer, styles.button]}
            onPress={() => {
              setIsBorderActive(false);
              getViewShot().then(() => {
                setIsBorderActive(true);
              });
            }}
          >
            <Text style={styles.btnText}>Enregistrer dans le pokedex</Text>
          </TouchableOpacity>
        )}

        {/* Add padding to allow scroll on card completion */}
        <View style={{ paddingBottom: "200%" }}></View>
      </ImageBackground>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  marginTop: {
    marginTop: 20,
  },
  container: {},
  cardBackground: {
    flex: 1,
    width: "100%",
    alignItems: "center",
  },
  card: {
    top: 0,
    left: 0,
    position: "relative",
  },
  cardInput: {
    borderWidth: 1,
    borderColor: "#000",
    position: "absolute",
  },
  cardHealth: {
    position: "absolute",
    fontWeight: "bold",
    fontSize: 12,
  },
  cardType: {
    // backgroundColor: "#000",
  },
  cardTypeIos: {
    flex: 1,
    zIndex: 3, // works on ios
    elevation: 3, // works on android
    backgroundColor: "#fff",
  },
  cardImage: {
    // backgroundColor: "#000"
  },
  cardDescription: {
    textAlignVertical: "top",
    fontSize: 10,
  },
  //Modal
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
  },
  modalView: {
    width: 300,
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  modalOption: {
    textAlign: "center",
    justifyContent: "center",
  },
  modalOptionButton: {
    height: 45,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 40,
    width: 200,
    borderRadius: 30,
    backgroundColor: "#bcbcbc",
  },
  resetButton: {
    backgroundColor: "#00b5ec",

    shadowColor: "#808080",
    shadowOffset: {
      width: 0,
      height: 9,
    },
    shadowOpacity: 0.5,
    shadowRadius: 12.35,

    elevation: 19,
  },
  closeModal: {
    right: 15,
    top: 15,
    position: "absolute", // add if dont work with above
  },
  // Camera
  cameraContainer: {
    width: "100%",
    height: "100%",
    position: "absolute",
    top: "10%",
    left: "8%",
  },
  // Progress bar
  progressBarContainer: {
    marginTop: 20,
  },
  // Button
  buttonContainer: {
    height: 45,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 20,
    width: 300,
    borderRadius: 30,
    backgroundColor: "transparent",
  },
  button: {
    backgroundColor: "#FAE16D",
    shadowColor: "#808080",
    shadowOffset: {
      width: 0,
      height: 9,
    },
  },
  btnText: {
    fontWeight: "bold",
  },
  noBorder: {
    borderWidth: 0,
  },
  dispNone: {
    display: "none",
  },
});

export default Add;
