import React, { useState, useEffect } from "react";
import {
  Image,
  ImageBackground,
  StyleSheet,
  Text,
  TouchableOpacity,
  TouchableOpacityComponent,
  View,
} from "react-native";
import useAbout from "../../hooks/useAbout";
import * as Battery from "expo-battery";

import images from "../../../assets";

const About = () => {
  const devNames = [
    images["christianAbout"],
    images["ludovicAbout"],
    images["guillaumeAbout"],
  ];

  const [levelBattery, setlevelBattery] = useState<number>(0);

  const [names, setNames] = useState<string[]>(
    Array(devNames.length).fill(images["hiddenPokeball"])
  );

  const changeIcon = (id: number) => {
    let newNames = [...names];
    newNames[id] = devNames[id];
    setNames(newNames);
  };

  useEffect(() => {
    console.log(names);
  }, [names]);

  useEffect(() => {
    Battery.getBatteryLevelAsync().then((value) => {
      let batteryLevel = Math.round(value * 100);
      setlevelBattery(batteryLevel);
    });
  }, [levelBattery]);

  const battery = async () => {};

  return (
    <View style={styles.container}>
      <ImageBackground
        source={images["legendaryDevs"]}
        resizeMode="cover"
        style={styles.cardBackground}
      />
      <Text style={styles.title}>
        Mais qui sont ces développeurs de génie ???
      </Text>
      <View style={styles.flex}>
        {names.map((name, id) => {
          return (
            <TouchableOpacity
              style={styles.imgDev}
              onPress={() => changeIcon(id)}
              key={`dev-${id}`}
            >
              <Image style={styles.img} resizeMode="contain" source={name} />
            </TouchableOpacity>
          );
        })}
      </View>

      <View style={styles.Imagecontainer}>
        <Text style={styles.title}>
          Statut de votre baterie {levelBattery} %{" "}
        </Text>
        {levelBattery >= 60 && (
          <Image style={styles.image} source={images["pikaLightning"]} />
        )}
        {levelBattery >= 30 && levelBattery < 59 && (
          <Image style={styles.image} source={images["pikaHappy"]} />
        )}
        {levelBattery > 0 && levelBattery < 29 && (
          <Image style={styles.image} source={images["pikaSleep"]} />
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  imgDev: {
    flex: 0,
    flexBasis: "30%",
    width: "30%",
    margin: 5,
    height: 150,
  },
  flex: {
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "center",
  },
  img: {
    width: "100%",
    height: "100%",
  },
  title: {
    textAlign: "center",
    fontSize: 24,
    marginTop: 15,
    marginBottom: 15,
  },
  cardBackground: {
    flex: 1,
    position: "absolute",
    top: 0,
    left: 0,
    height: "100%",
    width: "100%",
    opacity: 0.2,
  },
  container: {
    // alignItems: "stretch"
    flex: 1,
  },
  Imagecontainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  image: {
    height: 270,
    justifyContent: "center",
    resizeMode: "contain",
  },
});

export default About;
