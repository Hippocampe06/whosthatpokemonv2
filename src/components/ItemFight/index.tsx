import { Text, View, Image, StyleSheet, TouchableOpacity } from "react-native";

type ItemFightProps = {
  callMove: (move: Move) => void;
  pokemon: Pokemon;
  joueurType: string;
  hp: number;
};

const ItemFight = (props: ItemFightProps) => {
  const { callMove, pokemon, hp, joueurType } = props;

  return (
    <View style={styles.item}>
      <View>
        <Text style={styles.name}>{pokemon.name}</Text>
        <Image
          source={{
            uri: pokemon.sprites.other["official-artwork"].front_default,
          }}
          style={styles.img}
        />
      </View>

      <View style={styles.movesParent}>
        <Text style={styles.hp}>{hp} HP</Text>

        {pokemon.moves.map((move, id) => {
          if (joueurType === "joueur") {
            return (
              <TouchableOpacity
                key={`move-${id}`}
                onPress={() => {
                  callMove(move);
                }}
              >
                <Text style={styles.moveName}>{move.name}</Text>
                <Text style={styles.moveDamage}>{move.damage}</Text>
              </TouchableOpacity>
            );
          } else {
            return (
              <View key={`move-${id}`}>
                <Text style={styles.moveName}>{move.name}</Text>
                <Text style={styles.moveDamage}>{move.damage}</Text>
              </View>
            );
          }
        })}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  img: {
    width: 180,
    height: 180,
  },
  item: {
    flex: 1,
    flexDirection: "row",
  },
  name: {
    fontSize: 18,
    textTransform: "capitalize",
    textAlign: "center",
  },
  movesParent: {
    flexDirection: "column",
    justifyContent: "space-evenly",
    paddingTop: 50,
  },
  moveName: {
    fontSize: 20,
    fontWeight: "bold",
    marginBottom: 5,
    paddingLeft: 10,
  },
  moveDamage: {
    fontSize: 18,
    marginBottom: 25,
    paddingLeft: 10,
  },
  hp: {
    position: "absolute",
    top: 5,
    right: 5,
    fontSize: 24,
    fontWeight: "bold",
  },
});

export default ItemFight;
