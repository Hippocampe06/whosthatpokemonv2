import React, { useState, useEffect } from "react";
import { Image, StyleSheet, Text, View } from "react-native";
import useStorage from "../../hooks/useStorage";

import images from '../../../assets';

type ItemPokedexProps = {
  // card: Card
  cardFromStorage: CardFromStorage;
};

const ItemPokedex = (props: ItemPokedexProps) => {

    // const [type, setType] = useState<string>("");

    const { cardFromStorage } = props;
    
    const { image } = useStorage();
    const capitalize = (s: string) => (s && s[0].toUpperCase() + s.slice(1)) || "";

    useEffect(() => {
        // const newType = "cardType" + capitalize(props.card.type);
        // setType(newType);

    }, []);

    return (
      <View style={styles.container}>
        {/* <Text style={styles.title}>{props.card.name}</Text>
            <Text style={styles.hp}>{props.card.hp}</Text>
            <Image
                style={styles.img}
                source={images[type]}/>
            <Image
                style={styles.imgPokemon}
                source={{
                    uri: image
                }}/> */}
        <Image
          style={styles.imgPokemon}
          source={{
            uri: cardFromStorage.url,
          }}
        />
      </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexBasis:"40%",
        height: 500,
        padding: 15,
        margin: 10
    },
    title: {
        fontSize: 20,
        fontWeight: "bold"
    },
    hp : {
        fontSize: 11,
        position: "absolute",
        top: 20,
        right: 20
    },
    img: {
        width: 40,
        height: 40,
        position: "absolute",
        bottom: 10,
        right: 10
    },
    imgPokemon: {
        position: "relative",
        resizeMode: "contain",
        flex: 1
    }
})

export default ItemPokedex;