import {
  Dimensions,
  Image,
  ImageBackground,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";

import { AntDesign } from "@expo/vector-icons"; 

type EmptyItemPokedexProps = {
  userCardType: string;
  nav: any,
  user: any
};

import images from "../../../assets";
import { useNavigation } from "@react-navigation/core";

const EmptyItemPokedex = (props: EmptyItemPokedexProps) => {
  const { userCardType, nav, user } = props;
    console.log("nav", nav);
  const fixTypeBackground = (s: string) => {
    return s.replace("Type", "");
  };

  const navigation = useNavigation();

  return (
    <View style={styles.container}>
      <Text style={styles.text}>
        Vous n'avez pas de Pokémon dans votre Pokédex, vous pouvez en ajouter
        via la page Add. En attendant, voici cette carte vide correspondant à
        votre type :)
      </Text>
      <TouchableOpacity onPress={() => navigation.navigate("Add", { user: user })}>
        <ImageBackground
          source={images[fixTypeBackground(userCardType)]}
          style={styles.imgBack}
          resizeMode="contain"
        />
        <AntDesign name="addfile" size={60} color="black" style={styles.icon} />
      </TouchableOpacity>
       {/* Add padding to allow scroll on card completion */}
       <View style={{ paddingBottom: "150%" }}></View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
  },
  imgBack: {
    width: "90%",
    height: "100%",
    marginLeft: "10%"
  },
  text: {
    fontSize: 16,
    marginBottom: -40,
    paddingHorizontal: 30,
    textAlign: "justify"
  },
  icon: {
      position: "absolute",
      top: "30%",
      left: (Dimensions.get("screen").width / 2) - 30
  }
});

export default EmptyItemPokedex;
