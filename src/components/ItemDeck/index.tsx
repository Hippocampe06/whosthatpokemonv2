import { Image, Text, View, StyleSheet } from "react-native";


type ItemDeckProps = {
    pokemon: Pokemon
}

const ItemDeck = (props: ItemDeckProps) => {
    
    const { pokemon } = props;

    return(
        <View
            style={styles.item}>
            <Text
                style={styles.name}>
                {pokemon.name}
            </Text>
            <Image 
                source={{
                    uri: pokemon.sprites.other["official-artwork"].front_default
                }}
                style={styles.img}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    img: {
        width: 110,
        height: 110
    },
    item: {
        flex: 1
    },
    name: {
        textTransform: "capitalize",
        fontSize: 16
    }
});

export default ItemDeck;