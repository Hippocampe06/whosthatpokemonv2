import React, { useEffect } from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import { About, Add, Pokedex } from '../../pages';

import { MaterialCommunityIcons } from '@expo/vector-icons';
import { MaterialIcons } from '@expo/vector-icons'; 
import { Ionicons } from '@expo/vector-icons'; 
import { TouchableOpacity } from 'react-native';
import { FontAwesome5 } from '@expo/vector-icons';
import  useFirebaseLogin from "../../hooks/useFirebaseLogin"

const Tab = createBottomTabNavigator();

type BottomBarProps = {
    user : any,
    onClick : () => void 
}

const BottomBar = (props: BottomBarProps) => {
    
    const { logout } = useFirebaseLogin();
    const { user, onClick } = props;
    
    return (
      <React.Fragment>
        <Tab.Navigator
          screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
              let icon;

              switch (route.name) {
                case "Pokedex":
                  icon = (
                    <MaterialCommunityIcons
                      name="cards"
                      size={24}
                      color={color}
                    />
                  );
                  break;
                case "Add":
                  icon = (
                    <Ionicons
                      name="add-circle-outline"
                      size={24}
                      color={color}
                    />
                  );
                  break;
                case "About":
                  icon = (
                    <MaterialCommunityIcons
                      name="pokeball"
                      size={24}
                      color={color}
                    />
                  );
                  break;
                default:
                  icon = <MaterialIcons name="error" size={24} color={color} />;
                  break;
              }

              return icon;
            },
            tabBarActiveTintColor: "tomato",
            tabBarInactiveTintColor: "gray",
          })}
        >
          <Tab.Screen
            options={{
              headerRight: () => (
                <TouchableOpacity>
                  <FontAwesome5
                    style={{ marginRight: "5%" }}
                    onPress={() => logout()}
                    name="user-slash"
                    size={24}
                    color="black"
                  />
                </TouchableOpacity>
              ),
              // Cette ligne est une masterclass :)
              // 4 jours de recherche pour une seule ligne
              // Vive la vie de développeur
              unmountOnBlur: true,
            }}
            name="Pokedex"
            component={(props) => (
              <Pokedex onClick={onClick} user={user} {...props} />
            )}
          />
          <Tab.Screen
            name="Add"
            options={{ unmountOnBlur: true }}
            component={(props) => <Add user={user} {...props} />}
          />
          <Tab.Screen
            name="About"
            options={{ unmountOnBlur: true }}
            component={About}
          />
        </Tab.Navigator>
      </React.Fragment>
    );

}

export default BottomBar;
