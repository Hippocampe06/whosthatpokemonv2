import React, { useEffect, useState } from "react";

import { addDoc, collection, getDocs, query, where, getDoc, limit } from "firebase/firestore"; 

import { initializeApp } from "firebase/app";
import { getFirestore, doc, setDoc } from "firebase/firestore";
import { EmailAuthCredential } from "firebase/auth";
import useFirebaseLogin from '../useFirebaseLogin';

const firebaseConfig = {
    apiKey: "AIzaSyAcI6MbtSMzIz1XtRzfeAV2yY6dk9Teakw",
    authDomain: "whosthatpokemonv2.firebaseapp.com",
    projectId: "whosthatpokemonv2",
    storageBucket: "whosthatpokemonv2.appspot.com",
    messagingSenderId: "67906114245",
    appId: "1:67906114245:web:6b06db08977407a836d6ac"
};
// Initialize Firebase
const app = initializeApp(firebaseConfig);
  
const db = getFirestore();

const useFirestore = () => {

    const [ cards, setCards] = useState<undefined | Card[]>();
    const [ currentUserId, setCurrentUserId] = useState("");
    const [ currentUser, setCurrentUser] = useState("");
    const [ userCardType, setuserCardType] = useState("");
    const [ userId, setUserId] = useState("");
    

    const getAllCards = async() => {
        const querySnapshot = await getDocs(collection(db, "cards"));
        querySnapshot.forEach((doc) => {
            console.log(doc.id, " => ", doc.data());
        });
    }

    const getAllCardsFromUser = async() => {
        const q = query(collection(db, "cards"), where("idUser", "==", userId));
        const querySnapshot = await getDocs(q);
        let allCards: Card[] = [];
        querySnapshot.forEach((doc) => {
            allCards.push(doc.data() as Card);
        });
        setCards(allCards);
    }

    const getCurrentUser = async(user : any) => { 
        const q = query(collection(db, "users"), where("email", "==", user.email), limit(1));
        const querySnapshot = await getDocs(q);
        querySnapshot.forEach((doc) => {
                setCurrentUserId(doc.data().name);
                setuserCardType(doc.data().type);
                setUserId(doc.id);             
        });  
        setCurrentUser(user);
    }

    const setUserInDocument = async (userName: string, email: string, type: string) => {
        // Add a new document with a generated id.
        const docRef = await addDoc(collection(db, "users"), {
            name: userName,
            email: email,
            type: type
        });
        console.log("Document written with ID: ", docRef.id);
    }

    const createCard = async (name: string, hp: string, cardType: string, attackTitle1: string, attackTitle2: string, 
        attackDamage1: string, attackDamage2: string, attackDesc1: string, attackDesc2: string) => {
            console.log("DATA : ", userId, name, hp, cardType, attackDamage1);
        // Add a new document with a generated id.
        const docRef = await addDoc(collection(db, "cards"), {
            idUser: userId,
            name: name,
            hp: hp,
            type: cardType,
            attackLabel1: attackTitle1,
            attackDescription1: attackDesc1,
            attackDamage1: attackDamage1,
            attackLabel2: attackTitle2,
            attackDescription2: attackDesc2,
            attackDamage2: attackDamage2
        });
        console.log("Document written with ID: ", docRef.id);
    }

    useEffect(() => {
        getAllCardsFromUser();
    }, [currentUser]);

    return {
        cards,
        currentUserId,
        userCardType,
        userId,
        setUserInDocument,
        createCard,
        getCurrentUser
    }

}

export default useFirestore;