import { useNavigation } from "@react-navigation/native";

const useEndScreen = () => {
  const navigation = useNavigation();

  const replay = () => {
    navigation.navigate("Deck");
  };

  const launchFight = (selectedPokemon, iaPokemon) => {
    navigation.navigate("Fight", {
      pokemon: selectedPokemon,
      iaPokemon: iaPokemon,
    });
  }

  return {
    replay,
    launchFight
  };
};

export default useEndScreen;
