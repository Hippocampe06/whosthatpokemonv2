import { useEffect, useState } from "react";

const useDeck = () => {
  const [pokemons, setPokemons] = useState<Array<Pokemon>>([
    {
      name: "venusaur",
      id: 3,
      sprites: {
        other: {
          "official-artwork": {
            front_default:
              "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/3.png",
          },
        },
      },
      hp: 110,
      moves: [
        {
          name: "Cut",
          damage: 50,
        },
        {
          name: "Razor Leaf",
          damage: 55,
        },
      ],
    },
    {
      name: "charizard",
      id: 6,
      sprites: {
        other: {
          "official-artwork": {
            front_default:
              "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/6.png",
          },
        },
      },
      hp: 110,
      moves: [
        {
          name: "Dragon Claw",
          damage: 50,
        },
        {
          name: "Overheat",
          damage: 80,
        },
      ],
    },
    {
      name: "blastoise",
      id: 9,
      sprites: {
        other: {
          "official-artwork": {
            front_default:
              "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/9.png",
          },
        },
      },
      hp: 130,
      moves: [
        {
          name: "Ice Beam",
          damage: 60,
        },
        {
          name: "Hydro Pump",
          damage: 80,
        },
      ],
    },
    {
      name: "gengar",
      id: 94,
      sprites: {
        other: {
          "official-artwork": {
            front_default:
              "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/94.png",
          },
        },
      },
      hp: 120,
      moves: [
        {
          name: "Shadow Ball",
          damage: 80,
        },
        {
          name: "Sludge Bomb",
          damage: 50,
        },
      ],
    },
    {
      name: "snorlax",
      id: 143,
      sprites: {
        other: {
          "official-artwork": {
            front_default:
              "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/143.png",
          },
        },
      },
      hp: 180,
      moves: [
        {
          name: "Heavy Slam",
          damage: 50,
        },
        {
          name: "Outrage",
          damage: 80,
        },
      ],
    },
    {
      name: "articuno",
      id: 144,
      sprites: {
        other: {
          "official-artwork": {
            front_default:
              "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/144.png",
          },
        },
      },
      hp: 100,
      moves: [
        {
          name: "Ice Beam",
          damage: 50,
        },
        {
          name: "Blizzard",
          damage: 80,
        },
      ],
    },
    {
      name: "dragonite",
      id: 149,
      sprites: {
        other: {
          "official-artwork": {
            front_default:
              "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/149.png",
          },
        },
      },
      hp: 110,
      moves: [
        {
          name: "Outrage",
          damage: 80,
        },
        {
          name: "Dragon Claw",
          damage: 50,
        },
      ],
    },
    {
      name: "mew",
      id: 151,
      sprites: {
        other: {
          "official-artwork": {
            front_default:
              "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/151.png",
          },
        },
      },
      hp: 100,
      moves: [
        {
          name: "Focus Blast",
          damage: 80,
        },
        {
          name: "Psychic",
          damage: 50,
        },
      ],
    },
  ]);

  return {
    pokemons,
  };
};

export default useDeck;
