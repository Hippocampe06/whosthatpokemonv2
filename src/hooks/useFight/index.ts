import { useEffect, useState } from "react";

import { useNavigation } from "@react-navigation/native";

const useFight = () => {
  const navigation = useNavigation();

  const [step, setStep] = useState<Step>("null");
  const [player, setPlayer] = useState<Joueur>();
  const [ia, setIa] = useState<Joueur>();

  const [iaHp, setIaHp] = useState<number>(0);
  const [playerHp, setPlayerHp] = useState<number>(0);

  const callMove = (move: Move) => {
    let damage = move.damage;
    console.log("player attacked : ", damage);
    if (damage >= iaHp) {
      setIaHp(0);
      setStep("win");
    } else {
      setIaHp(iaHp - damage);
      setStep("playerended");
    }
  };

  const rdIntBetween = (min: number, max: number) => {
    // min and max included
    return Math.floor(Math.random() * (max - min + 1) + min);
  };

  const iaCallMove = () => {
    setTimeout(() => {
      const rnd = rdIntBetween(0, 1);
      const rndMoveDamage = (ia?.pokemon as Pokemon).moves[rnd].damage;
      console.log("ia attacked : ", rndMoveDamage);
      if (rndMoveDamage >= playerHp) {
        setPlayerHp(0);
        setStep("loose");
      } else {
        setPlayerHp(playerHp - rndMoveDamage);
        setStep("iaended");
      }
    }, 500)
  };

  useEffect(() => {
    switch (step) {
      case "playerended":
        iaCallMove();
        break;
      case "iaended":
        // playerTurn();
        break;
      case "iaturn":
        iaCallMove();
        break;
      case "win":
        console.log("wiiiiiin");
        setTimeout(() => {
          navigation.navigate("Win");
        }, 500);
        break;
      case "loose":
        console.log("loooooooose");
        setTimeout(() => {
          navigation.navigate("Loose");
        }, 500);
        break;
    }
  }, [step]);

  useEffect(() => {
    setStep("playerturn");
  }, [player]);

  return {
    callMove,
    step,
    player,
    ia,
    setStep,
    setPlayer,
    setIa,
    iaHp,
    playerHp,
    setIaHp,
    setPlayerHp,
  };
};

export default useFight;
