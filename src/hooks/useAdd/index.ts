import React, { useState, useEffect } from "react";

const useAdd = () => {
    const [card, setCard] = useState<null | undefined | Object>();

    return {
        card
    }
}

export default useAdd;