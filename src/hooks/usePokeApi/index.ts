import React, { useState, useEffect } from "react";
import axios from "axios";


const usePokeApi = () => {

    

    const getPokemon = (pokeName: string) => {
        axios.get(`https://pokeapi.co/api/v2/pokemon/${pokeName}`)
            .then(res => {
                console.log(res.data);
            })
    }

    return {
        getPokemon
    }

}

export default usePokeApi;