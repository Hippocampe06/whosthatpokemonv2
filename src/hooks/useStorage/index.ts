import React, { useEffect, useState } from "react";

import { collection, getDocs, query, where } from "firebase/firestore";

import { initializeApp } from "firebase/app";

import {
  getStorage,
  ref,
  getDownloadURL,
  uploadBytes,
  uploadBytesResumable,
  listAll,
  getMetadata,
} from "firebase/storage";
import { Alert } from "react-native";
import uuid from "react-native-uuid";
import { User } from "firebase/auth";
import useFirestore from "../useFirestore";

const firebaseConfig = {
  apiKey: "AIzaSyAcI6MbtSMzIz1XtRzfeAV2yY6dk9Teakw",
  authDomain: "whosthatpokemonv2.firebaseapp.com",
  projectId: "whosthatpokemonv2",
  storageBucket: "whosthatpokemonv2.appspot.com",
  messagingSenderId: "67906114245",
  appId: "1:67906114245:web:6b06db08977407a836d6ac",
};
const firebaseApp = initializeApp(firebaseConfig);
const storage = getStorage(firebaseApp);

const useStorage = () => {
  const [image, setImage] = useState<undefined | string>();
  const [progress, setProgress] = useState<undefined | number>();
  const [isUpload, setIsUpload] = useState(false);
  const [allUserImages, setAllUserImages] = useState<Array<CardFromStorage>>(
    []
  );

  // Upload image to firestore
  const uploadImageAsync = async (uri: any, userId: string) => {
    setIsUpload(true);

    const blob: Blob = await new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();
      xhr.onload = function () {
        resolve(xhr.response);
      };
      xhr.onerror = function (e) {
        console.log(e);
        reject(new TypeError("Network request failed"));
      };
      xhr.responseType = "blob";
      xhr.open("GET", uri, true);
      xhr.send(null);
    });

    // if le dossier n'existe pas, le créer, sinon
    let folderUserId = userId;

    // Upload file and metadata
    const storageRef = ref(
      storage,
      folderUserId + "/" + uuid.v4().toString() + ".png"
    );
    const uploadTask = uploadBytesResumable(storageRef, blob);

    // Listen for state changes, errors, and completion of the upload.
    uploadTask.on(
      "state_changed",
      (snapshot) => {
        // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
        setProgress((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
        console.log("Upload is " + progress + "% done");
        switch (snapshot.state) {
          case "paused":
            console.log("Upload is paused");
            break;
          case "running":
            console.log("Upload is running");
            break;
        }
      },
      (error) => {
        // A full list of error codes is available at
        // https://firebase.google.com/docs/storage/web/handle-errors
        switch (error.code) {
          case "storage/unauthorized":
            Alert.alert(
              "Erreur durant l'enregistrement de la carte",
              "Vous n'avez pas l'autorisation d'accéder à cet objet"
            );
            break;
          case "storage/canceled":
            Alert.alert(
              "Erreur durant l'enregistrement de la carte",
              "Vous avez annulé le téléchargement"
            );
            break;
          case "storage/unknown":
            Alert.alert(
              "Erreur durant l'enregistrement de la carte",
              "Une erreur inconnue s'est produite, veuillez recommencer"
            );
            break;
        }
      },
      () => {
        // Upload completed successfully, now we can get the download URL
        getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
          setIsUpload(false);
          Alert.alert(
            "Fichier enregistré avec succès",
            "Votre carte a bien été enregistrée dans le storage de Firebase"
          );
          console.log("File available at", downloadURL);
        });
      }
    );
  };

  const getImage = async () => {
    const bulbizarre = ref(storage, "bulbizarre.png");
    getDownloadURL(bulbizarre).then((url) => setImage(url));
  };

  const getAllCardsFromUser = async (userId: string) => {
    const userFolderRef = ref(storage, userId);

    let allUserCards: Array<CardFromStorage> = [];

    listAll(userFolderRef)
      .then((res) => {
        res.items.forEach((itemRef) => {
          getDownloadURL(itemRef).then((cardUrl) => {
            getMetadata(itemRef).then((metadata) => {
              let date = new Date(metadata.timeCreated);

              let newCard = {
                url: cardUrl,
                dateUploaded: date,
              } as CardFromStorage;

              let temp = Array.from(allUserCards).concat(newCard);

              setAllUserImages(temp);
              allUserCards.push(newCard);
            });
          });
        });
      })
      .catch((error) => {
        console.log("error throw : ", error);
        setAllUserImages([]);
      });
  };

  useEffect(() => {
    getImage();
    progress;
    isUpload;
  }, []);

  return {
    image,
    uploadImageAsync,
    progress,
    isUpload,
    getAllCardsFromUser,
    allUserImages,
    setAllUserImages
  };
};

export default useStorage;
