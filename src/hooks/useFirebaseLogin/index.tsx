import { useState } from "react";
import {
    getAuth,
    onAuthStateChanged,
    User,
    signInWithEmailAndPassword,
    signOut,
    createUserWithEmailAndPassword,
    sendPasswordResetEmail,
  } from "firebase/auth";
import useFirestore from "../../hooks/useFirestore"

const useFirebaseLogin = () => {

  // User
  const [user, setUser] = useState<User | null>(null);
  // Error message 
  const [errorMessage, setErrorMessage] = useState("");
  // Info message
  const [infoMessage, setInfoMessage] = useState("");
  // Set user in Firestore
  const { setUserInDocument } = useFirestore();


  // Check authentication
  const checkAuth = (): Promise<void> =>
    new Promise((resolve) => {
      const auth = getAuth();
      onAuthStateChanged(auth, (u) => {
        setUser(u);
        resolve();
      });
    });

  // Connect via email & password
  const connectEmailPassword = (email: string, password: string) => {
    const auth = getAuth();
      signInWithEmailAndPassword(auth, email, password)
      .then((value) => {
        console.log(value);
      })
      // if error, we send a custom error message
      .catch((error) => {
        setCustomErrorMessage(error.code);
      });
  };
    
  // Create new user
  const createUser = async (email: string, password: string, userName: string, type: string) => {
    const auth = getAuth();
    createUserWithEmailAndPassword(auth, email, password)
    .then(async (value) => {
      console.log(value);
      // Set document in firestore
      await setUserInDocument(userName, email, type);
      signOut(auth);
    })
    .catch((error) => {
      // if error, we send a custom error message
      setCustomErrorMessage(error.code);
    });
  }
  

  // Reset user password
  const resetPassword = (email: string) => {
    const auth = getAuth();
    sendPasswordResetEmail(auth, email)
    .then(() => {
      // Password reset email sent!
      let message = "Email envoyé avec succès. \nPenser à vérifier vos courriers indésirables";
      setInfoMessage(message);  
      setErrorMessage("");        
    })
    .catch((error) => {
      setCustomErrorMessage(error.code);
    });
  }

    // Logout  
    const logout = () => {
    const auth = getAuth();
    signOut(auth);
    };

    // Set custom error message
    const setCustomErrorMessage = (errorCode: string) => {
      switch (errorCode) {
        case "auth/invalid-email":
          setErrorMessage("Veuillez fournir un email au format 'exemple@mail.fr'")
          break;
        case "auth/weak-password":
          setErrorMessage("Le mot de passe doit comporter au moins six caractères")
          break;
        case "auth/wrong-password":
          setErrorMessage("Le mot de passe est incorrect")
            break;
        case "auth/email-already-in-use":
          setErrorMessage("L'utilisateur existe déjà")
          break;
        case "auth/user-not-found":
          setErrorMessage("L'utilisateur n'existe pas")
          break;
        default:
          setErrorMessage("Erreur, veuillez réessayer")
          break;
      }
    }

  
  return {
    user,
    errorMessage,
    infoMessage,
    checkAuth,
    connectEmailPassword,
    createUser,
    resetPassword,
    logout,
  };

}

export default useFirebaseLogin;

