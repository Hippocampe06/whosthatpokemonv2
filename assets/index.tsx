

// "assets/Cards"
import cardBug from  './Cards/card_bug.png';
import cardDark from  './Cards/card_dark.png';
import cardDragon from  './Cards/card_dragon.png';
import cardElectric from  './Cards/card_electric.png';
import cardFairy from  './Cards/card_fairy.png';
import cardFighting from  './Cards/card_fighting.png';
import cardFire from  './Cards/card_fire.png';
import cardGrass from  './Cards/card_grass.png';
import cardIce from  './Cards/card_ice.png';
import cardLight from  './Cards/card_light.png';
import cardNormal from  './Cards/card_normal.png';
import cardPsychic from  './Cards/card_psychic.png';
import cardRock from  './Cards/card_rock.png';
import cardSteel from  './Cards/card_steel.png';
import cardWater from  './Cards/card_water.png';


// "assets/Cards_Back"
import cardBack from  './Cards_Back/card_back.png';
import cardFan from  './Cards_Back/card_fan.png';


// "assets/Cards_Background"
import cardBackgroundDark from  './Cards_Background/card_background_dark.jpg';
import cardBackgroundDragon from  './Cards_Background/card_background_dragon.png';
import cardBackgroundElectric from  './Cards_Background/card_background_electric.jpg';
import cardBackgroundFairy from  './Cards_Background/card_background_fairy.jpg';
import cardBackgroundFighting from  './Cards_Background/card_background_fighting.jpg';
import cardBackgroundFire from  './Cards_Background/card_background_fire.jpg';
import cardBackgroundGrass from  './Cards_Background/card_background_grass.jpg';
import cardBackgroundNormal from  './Cards_Background/card_background_normal.jpg';
import cardBackgroundPsychic from  './Cards_Background/card_background_psychic.png';
import cardBackgroundSteel from  './Cards_Background/card_background_steel.jpg';
import cardBackgroundWater from  './Cards_Background/card_background_water.jpg';


// "assets/Cards_Type"
import cardTypeDark from  './Cards_Type/dark_ico.png';
import cardTypeDragon from  './Cards_Type/dragon_ico.png';
import cardTypeElectric from  './Cards_Type/electric_ico.png';
import cardTypeFairy from  './Cards_Type/fairy_ico.png';
import cardTypeFighting from  './Cards_Type/fighting_ico.png';
import cardTypeFire from  './Cards_Type/fire_ico.png';
import cardTypeGrass from  './Cards_Type/grass_ico.png';
import cardTypeNormal from  './Cards_Type/normal_ico.png';
import cardTypePsychic from  './Cards_Type/psychic_ico.png';
import cardTypeSteel from  './Cards_Type/steel_ico.png';
import cardTypeWater from  './Cards_Type/water_ico.png';



// "assets/Developpers"
import legendaryDevs from  './Developpers/3devs.jpg';
import christianAbout from  './Developpers/christian_about.png';
import guillaumeAbout from  './Developpers/guillaume_about.png';
import hiddenPokeball from  './Developpers/hidden_pokeball.png';
import ludovicAbout from  './Developpers/ludovic_about.png';

// "assets/Fight"
import win from './Fight/win.gif';
import loose from './Fight/loose.gif';


import backgroundFight from './Background/fight.jpg';
import backgroundLoose from './Background/lose.jpg';
import backgroundWinTest from "./Background/win_test.jpg";

// "assets/Battery"
import pikaCharge from './Battery/pika_charge.png';
import pikaHappy from './Battery/pika_happy.png';
import pikaLightning from './Battery/pika_lightning.png';
import pikaSleep from './Battery/pika_sleep.png';

export default {
  cardBug,
  cardDark,
  cardDragon,
  cardElectric,
  cardFairy,
  cardFighting,
  cardFire,
  cardGrass,
  cardIce,
  cardLight,
  cardNormal,
  cardPsychic,
  cardRock,
  cardSteel,
  cardWater,
  cardBack,
  cardFan,
  cardBackgroundDark,
  cardBackgroundDragon,
  cardBackgroundElectric,
  cardBackgroundFairy,
  cardBackgroundFighting,
  cardBackgroundFire,
  cardBackgroundGrass,
  cardBackgroundNormal,
  cardBackgroundPsychic,
  cardBackgroundSteel,
  cardBackgroundWater,
  cardTypeDark,
  cardTypeDragon,
  cardTypeElectric,
  cardTypeFairy,
  cardTypeFighting,
  cardTypeFire,
  cardTypeGrass,
  cardTypeNormal,
  cardTypePsychic,
  cardTypeSteel,
  cardTypeWater,
  legendaryDevs,
  christianAbout,
  guillaumeAbout,
  hiddenPokeball,
  ludovicAbout,
  win,
  loose,
  backgroundFight,
  backgroundLoose,
  backgroundWinTest,
  pikaCharge,
  pikaHappy,
  pikaLightning,
  pikaSleep,
};
