# Who's That Pokemon V2 ! <img src="./assets/logo.png" alt="wtp logo" title="WTP" align="right" height="35" />

**WhosThatPokemonV2** est un projet en ReactNative qui permet de créer des cartes pokémon personnalisées et de créer des decks à partir de vos cartes ou de cartes officielles Pokémon.

</br>

## Lancement du projet

</br>

```bash
git clone https://gitlab.com/Hippocampe06/whosthatpokemonv2.git
cd WhosThatPokemonV2
npm install
expo start
```

</br>

## Fonctionnalités

<br>

| Tâches | Description | Status | Valeur |
|-------|-------------|--------|-------|
| ```Utilisation d'un outil de versioning et de gestion de projet de type Kanban``` | Utilisation de Gitlab et de Trello (Ajout d'extensions Trello afin d'affiner la gestion de projet => Priorités des cartes, timer, dépendances / point de blocage entre les tâches, réactions sur les tâches) | Fait | 5pts |
| ```Application Android / Ios``` | Build de l'application sous Android et ios avec Expo (React Native)<br> => Testé sur Samsung S20 Api 30 version android OS 11 - R <br> => Testé sur tablette Lenovo Api 24 version android 7 OS Nougat <br> => Testé sur Iphone 13 ios 15.0.1 | Fait | 5pts |
| ```Application conforme à l'énoncé et fonctionnelle``` | - | - | 5pts |
| Icone et Splashscreen | Ajout de l'icone pokéball et d'un splash screen lors du lancement de l'application | Fait ||
| CRUD ou vrai fonctionnalité | Création d'une carte pokémon personnalisée et enregistrement de l'image dans la galerie et dans le Storage Firebase ainsi que dans Firestore | Fait ||
| Utiliser un ou plusieurs composants natifs | Splashscreen => Mise en place d'un splashscreen personalisé au lancement de l'application<br><br> Image picker => Sélection d'une image depuis la galerie du téléphone pour la vue de création de carte<br><br> Caméra => Gestion des autorisations de la caméra pour prendre une photo dans la vue de création de carte, ajout des options fermeture et prise de photo en mode selfie<br><br> Galerie/Stockage => Enregistrement du composant "Carte" ainsi que de tout ces champs afin d'enregistrer l'image dans la galerie du téléphone (+ gestion des autorisations et des messages d'erreurs)<br><br> Baterie => Relevé du niveau de baterie et affichage d'un Pikachu correspondant sur la page "About" <br>Entre 0 et 30% --> Pikachu fatigué<br>Entre 30 et 60% --> Pikachu normal<br>Entre 60 et 100% --> Pikachu énergique | Fait ||
| Appels API (Rest/GraphQL/Firebase) | Requêtes vers firebase storage et firestore pour la gestion de l'authentification du stockage des données | Fait ||
| ```Mise en place du Deeplink - Partage des cartes entre utilisateurs via deeplink``` | Abandonné trop complexe à mettre en place pour une application react native en lien avec les contraintes de notre application | Abandonné | 2pts |
| ```Ajouter des fonctionnalités et faire preuve d'initiative``` | - | Fait | 3pts |
| Création de compte et connexion sur l'application | Création de compte & gestion de l'authentification via Firabase Authentication. Gestion des messages d'erreurs (invalid-email, weak-password, wrong-password etc.). Gestion du mot de passe oublié avec l'envoi d'un mail permettant la réinitialisation de celui-ci <br><br> Prise en compte du type (Feu, Eau, Électrique etc) et du pseudo de l'utilisateur pour l'afficher sur la page d'accueil et personaliser l'affichage de l'application en fonction de la personne connectée :  <br><br>Affichage d'une carte du type sélectionné sur le pokédex  <br> Background du type sélectionné sur l'interface de combat  <br> Affichage du pseudo et des icônes du type sur la page "Pokédex" <br> <br> Passage de l'utilisateur en props dans l'ensemble de l'application pour le lier à son pokédex | Fait | ``` Initiative ``` |
| Mode combat contre une IA | Création d'un mode de combat. Vous pouvez choisir entre 8 différents pokémon et lancer la partie ! Choisissez ensuite l'attaque que vous souhaitez lancer à tour de rôle. A l'issue de votre combat, vous obtiendrez une page vous permettant de rejouer ou de quitter la partie | Fait| ``` Initiative ``` |
| Mode création de carte personnalisée | Gestion de la partie responsive pour l'affichage de la carte Pokémon sur différents appareils calculé selon le ratio de l'écran et de la carte. <br> <br>Personnalisation de l'ensemble des champs de la carte Pokémon (nom, HP, type, image, titres d'attaque, dégats d'attaque, descriptions d'attaque)<br> <br>Changement de l'environement de la carte lors d'un changement de type. C'est à dire, mise à jour du background + modification du corps de la carte <br> <br> Enregistrement de l'image au choix, soit galerie soit caméra au travers d'un modal<br> <br> Sauvegarde de la carte : <br> Via Firestore => Sauvegarde des champs de la carte en vue d'une hypothétique exploitation  <br>Via Firebase Storage => Afin d'avoir la carte au format png sur le pokédex de l'application <br>Via le stockage local => Afin d'enregistrer la carte en galerie pour un usage personel  <br> <br> NB: Les images sont enregistrées au format png depuis le composant viewShot sur un évenement déclenché par un bouton| Fait| ``` Initiative ``` |
| Partie "About" de l'application | La partie "About" fait référence aux développeurs ayant contribué au projet au travers de cartes à découvrir en cliquant dessus... | Fait | ``` Initiative ``` |
| Architecture de l'application | L'architecture de notre application est basée sur une architecture type du framework React-native. C'est à dire découpée en props, hooks, pages, assets  | Fait | ``` Initiative ``` |
| Style graphique de l'application | Le style de l'application se base sur la colorimétrie des cartes Pokémon tout en intégrant une navigation fluide et intuitive basée sur l'application "Who's that Pokemon" V1. cf: Capture d'écran de la maquette plus bas dans ce document | Fait | ``` Initiative ``` |

</br>

## Screenshots </br></br>
### Guide </br></br>

<img src="./readme_locals/app/Screenshots/createCard.png" alt="Create card" width="100%"/>

</br></br>

### Différents niveaux de batterie </br></br>
<img src="./readme_locals/app/Screenshots/batteryHight.png" alt="Hight level" width="200"/>
<img src="./readme_locals/app/Screenshots/batteryMedium.png" alt="Medium level" width="200"/>
<img src="./readme_locals/app/Screenshots/batteryLow.png" alt="Low level" width="200"/>

</br></br>

### Page de l'application 

</br></br>

<img src="./readme_locals/app/Screenshots/register.png" alt="Register" width="200"/>
<img src="./readme_locals/app/Screenshots/login.png" alt="Login" width="200"/>
<img src="./readme_locals/app/Screenshots/forgetPassword.png" alt="Forget password" width="200"/>
<img src="./readme_locals/app/Screenshots/pokedex.png" alt="Pokdex" width="200"/>
<img src="./readme_locals/app/Screenshots/AddCard.png" alt="Add card" width="200"/>
<img src="./readme_locals/app/Screenshots/teamBuilder.png" alt="Team builder" width="200"/>
<img src="./readme_locals/app/Screenshots/fight.png" alt="Fight" width="200"/>
<img src="./readme_locals/app/Screenshots/win.png" alt="Win" width="200"/>


</br></br>

### Architecture du projet

<br>
<img src="./readme_locals/app/Screenshots/architecture.png" alt="architecture" width="200"/>

<br>

## Gifs de l'application sous android
</br></br>

<img src="./readme_locals/app/gifs/WTPv2About.gif" alt="About" width="200"/>
<img src="./readme_locals/app/gifs/WTPv2SavePokedex.gif" alt="save card pokedex" width="200"/>
<img src="./readme_locals/app/gifs/WTPv2InvalidCredentials.gif" alt="Invalid credentials" width="200"/>
<img src="./readme_locals/app/gifs/WTPv2Fight.gif" alt="Fight" width="100%"/>
<img src="./readme_locals/app/gifs/WTPv2SaveCardCamera.gif" alt="Save card camera" width="100%"/>
<img src="./readme_locals/app/gifs/WTPv2SaveCardGallery.gif" alt="save card gallery" width="100%"/>

<br>
<br>

## Gifs de l'application sous ios
</br></br>

<img src="./readme_locals/app/gifs/about_ios.gif" alt="About" width="200"/>
<img src="./readme_locals/app/gifs/add_ios.gif" alt="Add" width="200"/>
<img src="./readme_locals/app/gifs/fight_ios.gif" alt="Fight" width="200"/>

<br>
<br>

## Auteurs

<br>

| ![Guillaume Cerdan](./readme_locals/profile/Guillaume_CERDAN.png) | ![Christian Hecquet](./readme_locals/profile/Christian_HECQUET.png) | ![Ludovic Flament](./readme_locals/profile/Ludovic_FLAMENT.png) |
|:--:| :--:| :--:| 
| *Guillaume Cerdan* | *Christian Hecquet* | *Ludovic Flament* |

<br>

## Retour d'experience 
<br>

Au travers de la refonte du projet Who’s That Pokemon (originalement développé en Kotlin, natif Android) avec le framework React Native, nous avons énormément appris sur l’utilisation du framework ainsi que sur le langage TypeScript.
Nous avons su intégrer une architecture simple et maintenable au cours du développement de l’application.
Suite à un grand nombre de fonctionnalités développées, nous avons rencontré de nombreuses complications.

La première aura été la prise en main du framework ainsi que du langage TypeScript et ses subtilités. Cependant, après quelques semaines, nous sommes maintenant bien plus à l’aise avec le framework et capables d’avance bien plus rapidement.

De plus, la gestion du responsive lors de la création des cartes aura été compliquée. En effet, il est très compliqué d’assurer le responsive lorsque beaucoup d’éléments de la vue sont en position absolue. Nous aurons réussi à faire une version la plus proche possible de la réalité, non sans encombres !

L’enregistrement en PNG d’une carte depuis une balise View aura été compliqué aussi à mettre en place. 

La mise en place de l’utilisation de la galerie pour le stockage des images.

Sur certaines fonctionnalités, nous avons dû gérer les différentes plateformes, en effet certains packages ne sont fonctionnels que sur un OS ou l’autre.

Nous avons aussi mis plus de temps que prévu afin de bien saisir comment partager des données entre :<br>
Composants / Composants<br>
Composants / Hooks<br>
Hooks / Hooks



Ensuite, nous avons eu du mal à gérer plusieurs menus dits contextuels dans l’application. Par exemple, lorsque nous allons sur la partie Combat de l’application, nous n’avons plus accès au reste de l’application via la BottomBar. Cette gestion aura été complexe à mettre en place mais nous aurons su trouver une architecture maintenable et simple.

<br>

## Maquette "Who's that Pokemon" V1 
<br>
<img src="./readme_locals/app/Screenshots/wtp1_1.jpg" alt="1" width="200"/>
<img src="./readme_locals/app/Screenshots/wtp1_2.jpg" alt="2" width="200"/>
<img src="./readme_locals/app/Screenshots/wtp1_3.jpg" alt="3" width="200"/>
<img src="./readme_locals/app/Screenshots/wtp1_4.jpg" alt="4" width="200"/>

## Liens et références

<br>

### Gestion de projet

<br>

* ```Gitlab :``` https://gitlab.com/Hippocampe06/whosthatpokemonv2
* ```Trello :``` https://trello.com/invite/b/tiNCGrst/91945827c73f22a6b93e2953c7d77505/whos-that-pokemon-v20

<br>

<img src="./readme_locals/app/Screenshots/trello.png" alt="trello"/>
<img src="./readme_locals/app/Screenshots/cardTrello.png" alt="Card trello" width="60%"/>


<br>

### Liens et références techniques

<br>

* ```API Pokémon :``` https://pokeapi.co/docs/v2
* ```Enregistrer dans la galerie :``` https://www.farhansayshi.com/post/how-to-save-files-to-a-device-folder-using-expo-and-react-native/
* ```Enregistrer une vue en tant qu'image :``` https://www.youtube.com/watch?v=CEB0QepurQY
* ```Librairie react-native-view-shot pour capturer l'écran du téléphone et/ou d'un composant :``` https://github.com/gre/react-native-view-shot
* ```Expo doc image picker :``` https://docs.expo.dev/versions/latest/sdk/imagepicker/
* ```Expo doc camera :``` https://docs.expo.dev/versions/v44.0.0/sdk/camera/
* ```Prendre les photos avec la caméra :``` https://stackoverflow.com/questions/52707002/how-to-snap-pictures-using-expo-react-native-camera