


type Card = {
    attackDamage1: string,
    attackDamage2: string,
    attackDescription1: string,
    attackDescription2: string,
    attackLabel1: string,
    attackLabel2: string,
    hp: string,
    idUser: string,
    name: string,
    type: string
}

type CardFromStorage = {
    url: string,
    dateUploaded: Date
}

type Move = {
    name: string,
    damage: number
}

type Pokemon = {
    id: number,
    name: string,
    sprites: {
        other: {
            "official-artwork": {
                front_default: string
            }
        }
    },
    hp: number,
    moves: Move[]
}

type TypeJoueur = "ia" | "joueur"

type Joueur = {
    type: TypeJoueur,
    pokemon: Pokemon | Object
}

type Step = "null" | "start" | "playerturn" | "playerended" | "iaturn" | "iaended" | "endgame" | "win" | "loose";

